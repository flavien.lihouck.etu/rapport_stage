Internship report: Abstract

My internship subject was working on a tool aiming to reconstruct a DNA sequence from the output of a sequencer, a machine used to extract the genetic information by cutting it into smaller fragments called reads.  
These reads are chains of characters coming from any part of the sequenced subject genome, with little to no informations on how to piece them back together
These sequencers are also prone to mistakes, and the reads need to be put back together to be useful. This makes correcting and assembling reads an important topic in the bioinformatics field.  
My tool takes raw reads as an input, uses a mapping tool to pre-align them, then group them together into Multiple Sequence Alignments using many different tools. For each of the resulting MSAs, we calculate a consensus sequence, we then realign these consensus together into a Meta-MSA, in order to be able to process a new and final meta-consensus.  
The strategy upon which the project is based stems from the fact that several tools exist to align reads, and those tools differs in the strategies and heuristics used, which results in them struggling with different types of problems, and, by making this meta-consensus, we attempt to use these tools strongsuits' to correct eachother's flaws.  
The tool itself is a Snakemake pipeline, a python-based tool to link together scripts' inputs and outputs in order to process a large amount of files easily, mixing scripts I wrote, ressources from Coralie Rohmer, a colleague, and existing tools.  
One of the focus of this work was to make the tool both easy to use and to iterate upon, by making the input and command relatively simple and easy to understand while still allowing for people to easily add support for new alignement tools, or new consensus strategies. This tool is intended to be part of an upcoming article.
The results appears to show that the MSA tools efficiency impacts heavily on the meta-consensus, and that cutting specific tools from the pipeline could be beneficial overall, but this tool seems to provide overall better result than each individual results.  

Flavien Lihouck