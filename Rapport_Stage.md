*Titre*
======

Lihouck Flavien

Licence 3 Informatique parcours Info

Du 02/05 au 29/07

Laboratoire CRIStAL + logo + Batiment Esprit, Av. Paul Langevin, 59650 Villeneuve-d'Ascq

Université de Lille + logo + 42 Rue Paul Duez, 59000 Lille

Limasset Antoine

Allegraud Pierre


\pagebreak

# Remerciements (0.5p)

Je tiens tout d'abord a remercier Antoine Limasset, chercheur dans l'équipe Bonsaï pour son encadrement tout au long de mon stage, ainsi que Pierre Allegraud pour l'encadrement pédagogique, les conseils et soutien qui ont permis d'assurer le bon déroulement de ce stage. Je tiens également à remercier toute l'équipe Bonsaï, en particulier Coralie Rohmer, doctorante et future docteure en bioinformatique, pour le rôle qu'elle a joué dans la qualité de l'accueil, et l'intégralité de son travail sans lequel ce projet n'aurait jamais eu lieu.

\pagebreak
# Résumés (1p)

## Fr (0.5p ~ 350 mots)

## En (0.5p ~ 350 mots  !! 13 juin !)

\pagebreak

# Sommaire (1p)

AUTO ?

\pagebreak
# Intro (1p)

Etant intéressé par la recherche en général, et ayant eu l'occasion de travailler avec Antoine Limasset dans le cadre de l'option Recherche, il m'a semblé intéressant de faire un stage dans la continuité de ce projet. Entre temps, un autre projet dans une thématique proche à été mis en avant, et c'est finalement sur ce projet que j'ai eu l'occasion de travailler. Ce donc dans le cadre du projet Méta-consensus que j'ai commencé mon stage au sein de l'équipe Bonsaï.
Le sujet de mon stage est le dévelloppement d'un outil de Bioinformatique visant à traiter et corriger des séquences issues de données biologiques brutes dans le but de reconstruire une séquence ADN la plus proche possible de la séquence d'origine. Mon travail entre dans le cadre d'études sur des outils pour la dernière technologie de séquencage, qui est un domaine important au sein de la Bioinformatique.
Il s'inscrit plus particulièrement dans la continuité d'un travail réalisé auparavant par une collégue doctorante, Coralie Rohmer, sur un outil pour réaliser le benchmark d'outils d'Alignement Multiple de Séquences, de même qu'un retour sur l'état de l'art sur les outils de MSA. De ce travail, l'idée de mettre en commun les points forts des meilleurs outils de MSA pour obtenir une séquence de la meilleure qualité possible. La réalisation de consensus à partir d'une MSA était déjà un concept éprouvé, mais pour l'instant, personne n'avait mis en place l'idée de combiner ces résultats afin de réaffiner les résultats obtenus. C'est ainsi que le projet de pipeline méta-consensus est apparu. Comme les outils de bioinformatique évoluent rapidement et que l'Alignement Multiple est un domaine actif, il est important que le pipeline puisse facilement être mis à jour pour implémenter de nouveaux outils. De plus, l'objectif du développement de cet outil étant de potentiellement faire l'objet d'un futur papier, et donc d'être utilisé de façon concrete par des Bioinformaticiens, voire des Biologistes, il est nécessaire de rendre l'utilisation de l'outil relativement accessible et intuitive. Cela a posé des questions d'ergonomie et de design tout au long de la conception. 

\pagebreak
# Contexte (3p)

$Ceci est un copié-collé sale du site de CRIStAL, il faut le nettoyer $ 

Mon stage se déroule au sein de CRIStAL (Centre de Recherche en Informatique, Signal et Automatique de Lille), une unité mixte de recherche (UMR 9189) résultant de la fusion du LAGIS (Laboratoire d’Automatique, Génie Informatique et Signal - UMR 8219) et du LIFL (Laboratoire d’Informatique Fondamentale de Lille - UMR 8022) pour fédérer leurs compétences complémentaires en sciences de l’information. Ce laboratoire regroupe plus 470 membres (230 permanents et près de 240 non permanents) dont 28 permanents CNRS et 23 permanents Inria.
Les activités de recherche de CRIStAL concernent les thématiques liées aux grands enjeux scientifiques et sociétaux du moment tels que : BigData, logiciel, image et ses usages, interactions homme-machine, robotique, commande et supervision de grands systèmes, systèmes embarqués intelligents, bio-informatique… avec des applications notamment dans les secteurs de l’industrie du commerce, des technologies pour la santé, des smart grids.
Les chercheurs sont répartis selon leur thématique(s) de recherche au sein de groupes thématiques, eux-mêmes découpés en plusieures équipes de recherche.

L'équipe dans laquelle j'ai eu la chance de réaliser mon stage est l'équipe Bonsaï, une équipe de Bioinformatique membre du groupe Modélisation pour les Sciences du Vivant (MSV).
Cette équipe, dirigée par Hélène Touzet, travaille majoritairement sur l'algorithmie des séquences, appliquée notamment à l’annotation des génomes, l’analyse des données de séquençage à haut débit, la métagénomique, la structure des gènes et des génomes, les ARN non-codants, les peptides non ribosomiques. Les méthodes employées viennent de l’algorithmique discrète, empruntant à l’algorithmique du texte, la théorie des graphes, les structures d’index… Ce travail se matérialise par la diffusion de logiciels libres, et la validation sur des données biologiques. 

La mission que l'on m'a confiée consiste au dévelloppement d'un outil visant à corriger et reconstruire des séquences complétes d'ADN à partir de données brutes, utilisant une stratégie de consensus d'outils.

\pagebreak
# Contribution (12-24p)

## Présentation Bio

###  ADN -> info génétique -> application : Agro/Médecine/...

Dans l'ensemble du vivant, l'information génétique, ou génome, servant à coder l'intégralité des caractères exprimés par un individu est porté par les chromosomes. Le nombres de chromosomes diffère selon les espèces, de même que l'organisation de ceux-çi.   
Les chromosomes sont composés d'Acide Désoxyribonucléique (ADN). L'ADN lui même est composé de deux brins, c'est à dire deux chaines de molécules appelés nucléotides. Ces nucléotides, ou bases,  existent dans l'ADN sous 4 formes différentes, respectivement l'Adénine (A), la Cystosine (C), la Guanine (G) et la Thymine (T).
Les bases s'apparient un A et toujours en face d'un T, un C en face d'un G. Cette propriété nous permet de lire un brin et de recomposer le brin opposé.   
Pour ces raisons, on peut comprendre plutôt intuitivement l'approche utilisée largement en Bioinformatique, qui consiste à représenter cette information génétique comme un seule chaîne de caractères composée de A, C, G et T.   
L'ADN est composé de sections impactant un caractère spécifique (couleurs des yeux, dégradation de sucre, ... ), ces sections sont dites codantes et sont généralement appelé gènes, les autres sections sont dites non codante.

Comme cette information permet de lier l'expression d'un caractère avec une information concrete, les applications de l'étude du génome peuvent présenter des intérets dans de nombreux domaines, telles que l'Agroalimentaire (selection plus précise de plants présentant tel avantage au sein d'une espéce, une meilleure résistance à une maladie, une plus grande production par pied , ... ), la Médecine (detection d'un facteurs de risques pour une affliction donnée, permettant de préciser les risques par rapport aux antécédants familiaux, ...) et la Biologie (étude phylogénétique, c'est-à-dire étude de l'évolution du génome dans l'ensemble du vivant, en comparant le génome d'espéces "proches" dans l"espoir de retracer les différences d'évolution, ...). 
Cela rends la correction et la précision de celle-çi importante, ce qui a motivé l'idéee de travailler sur cet outil.

###   Séquencage : substring shuffled

L'information génétique contenue à l'intérieur de cellules n'est pas accessible en l'état. La méthode utilisée pour extraire les séquences de nucléotides depuis les cellules est appelée séquencage. Les outils actuels ne permettent de lire qu'une fraction du génome à la fois (de l'ordre de la centaine de bases ou du millier de bases selon la technologie). En comparaison, le génome humain est de l'ordre du milliard de bases. On ne peut donc accéder qu'a des fragments du génome dont la position dans le génome est inconnue et aléatoire, ces fragments sont appelé \textit{reads}.

### Alignment / Assembly

Afin de pouvoir utiliser ces reads, il est donc nécessaire de retrouver la position d'origine de chaque read, dans l'espoir de pouvoir reconstruire la séquence compléte utilisable.

Pour ce faire, il existe deux solutions, l'Alignement ou l'Assemblage, et le "choix" de l'un par rapport à l'autre dépends des données préliminaires. 

En séquencant plusieures copies d'un même génome on va pouvoir obtenir des reads qui partagent des extrémités commune, on dit que les reads partagent un \textit{overlap}. En utilisant ces overlaps, il est possible en comparant les reads deux à deux de trouver une position relative entre ces reads. Cette position relative permet ensuite d'empiler les reads, et en fusionnant les reads partageant un \textit{overlap} on va pouvoir reconstruire le génome d'origine , ce qui est appelé l'assemblage de génome. Cette solution présente l'avantage de fonctionner à partir de reads uniquement, mais l'ensemble des comparaisons deviens très vite couteuse quand le nombre et la taille des reads augmente.

Comme les études du génome ont été un sujet depuis longtemps, de nombreuses espéces ont déjà été étudiées de manière plus ou moins étendue, et ce cumul de résultats à permis de créer ce que l'on appelle génome de référence.
L'Alignement consiste à prendre l'ensemble des reads, en recherchant leur position par rapport au génome de référence.  Avoir un génome de référence permet de simplement aligner les reads sur les zones qui correspondent le plus, ce qui limite le nombre de comparaisons et donc le coût pour obtenir un résultat similaire à l'Assemblage. Cependant, cela necessite l'existence d'une référence, ce qui n'est pas le cas pour une grande partie du vivant. De plus, comme les références ont étés assemblées par le passé, à partir de reads plus courts, il existe probablement des biais (comme il existe une diversité au niveau d'une espéce, quel est le cas "de référence", comment cette moyenne a-t-elle été réalisée), et des zones moins précises (le génome humain par exemple, n'as été "terminé" que cette année malgrè le focus particulier dont il est sujet)

Je reviendrais plus en détail sur l'aspect concret que prends l'assemblage/ l'alignement dans le pipeline au niveau des détails techniques.

###    Third Gen Seq -> long reads -> avancées

La nouvelle génération de reads est bien plus longue que la précédente, ( 10k bases contre 200-300 bases pour la génération précédente ), ce qui est un avantage majeur, qui a déjà permis des avancées remarquables. $En effet, grâce aux long reads, le génome Humain est désormais une séquence compléte \cite{Human_genome DOI: 10.1126/science.abj6987 }, ce qui était impossible jusqu'alors. En effet, le génome humain contients un fort taux de répétitions, ce qui fait qu'il était impossible de retrouver certaines parties du génome avec des reads plus courts, car ceux-çi ne se différenciaient pas assez pour être placés sur la bonne position. $ Je sais pas à quel point c'est ça ou si j'ai manqué un truc, faut que je relise plus le papier $ 
De plus, la longueurs de ces reads permet d'envisager la possibilité de réussir à séparer les versions des chromosomes chez les espéces a 2 (ou plus) copies de chaque chromosomes, copie issues des parents de l'individu (2 dans le régne animal, mais potentiellement beaucoup plus chez les végétaux par exemple). C'est ce que l'on appelle le phasing d'Haplotype. Cela permettrait de détecter précisément l'organisation de ces différences, et dans la finalité, permettrait peut-être découvrir des gènes plus complexes que l'on ne pouvait comprendre et détecter sans ce phasing... Ce sont tant de raisons de travailler sur ces longs reads, qui sont pour l'instant le modéle le plus prometteur.
Les longs présentent cependant des contraintes différentes des reads de la génération précédente, de part leur taille plus élevée, des contraintes d'espaces rendent certaine méthodes utilisées par le passé inadaptées, voir impossible, avec un coût en mémoire par read bien plus élevé.

### Taux d'erreurs

En plus de ces problèmes liés à leur taille, les longs reads ont également un désavantage majeur par rapport à leur prédécesseurs. En effet, les lectures des séquenceurs peuvent être sujettes à des erreurs, celles-çi se présentant sous trois types :
\begin{itemize}
\item Substitution: une base est 'mal-lue', et remplacée par une autre base
\item Suppression: une base est 'ignorée', créant un décalage dans le reste de la séquence, à laquelle il manque une base
\item Insertion: une base est lue 'à tort', créant un décalage, avec une séquence contenant une base en trop
\end{itemize} 
Pour les reads de 3eme génération, le taux d'erreur est de l'ordre de 5 à 10%, là ou les shorts reads ont un taux d'erreur bien inférieur à 1%. Ce haut taux d'erreur nécessite donc un travail de correction de l'information afin de pouvoir l'utiliser.

###    Redondance

Afin de pouvoir détecter et corriger ces erreurs, la stratégie utilisée consiste a séquencer un nombre de copie du génome important dans le but d'obtenir un grand nombre de reads pour chaque position du génome. Si les erreurs sont bien répartie de manière aléatoire et peu fréquentes, pour une position donnée on aura beaucoup plus de lectures sans erreur que de lecture avec une erreur, ce qui permettra donc de distinguer les erreurs des variations haplotypiques. 

Une difficulté suplémentaires dans la détection/correction des erreurs viens du fait que ces erreurs touchent une seule base isolée, ce qui peut les rendre difficiles à différencier d'une situation 'normale', dans le cas où les copies d'un même chromosome sont différentes à une position donnée (Single Nucleotid Polymorphisme). Ce type de différences interviennent sur environ 1 base toutes les 1000 (0,1%) chez l'Humain. Ces polymorphismes sont importants car ils sont à l'origine des expressions de caractères différentes d'un individu à un autre, et donc de la diversité génétique d'une espéce. Ces informations sont donc importantes, et demandent une attention particulière.

###    consensus

En ayant accumulé suffisement de reads, il est statistiquement possible pour une position donnée de trouver la ou les bases correspondantes en réalisant un consensus. Comme le taux d'erreur est globalement inférieur à 10%, cela reviens à dire qu'avec suffisement de reads on devrait tendre vers 90% de bases correctes. On peut donc considérer que si le taux de présence d'une base à la position est supérieure à un seuil défini, c'est statistiquement la base correcte. Dans le cas de polymorphisme sur la position donnée, on peut considérer que ce sont les X bases les plus présentes, où X correspond au nombre de copies attendues pour un chromosome donné, dont la somme de leur taux de présence dépasse le seuil, qui sont correctes.
Fixer un seuil est une décision importante, car un seuil trop bas prends le risque d'ignorer un potentiel polymorphisme, et un seuil trop haut risque de ralentir voire d'ignorer certaines bases pour lesquels les taux d'erreurs seraient trop élevés.

Cette stratégie de consensus est au coeur du fonctionnemet du pipeline.
    
###    MSA -> NP-hard

L'une des façons les plus simple de traiter les reads serait de les avoir tous alignés proprement, avec l'ensemble des positions contenant soit la valeur du read pour la position, soit un vide ("gap"). On appéle cette forme d'alignement, de même que le fait de le calculer un Alignement Multiple de Séquence (Multiple Sequence Alignment ou MSA).
La réalisation d'un alignement multiple de séquences est un problème combinatoire, avec un nombre de possibilités qui augmente avec la taille et le nombre de séquences, de la forme O(taille(pow(nombre_séquence)) dans l'approche naive. Le problème étant NP-complet, il a été nécessaire de trouver des heuristiques pour arriver un approximation du résultat.
De ce besoin, beaucoup d'équipes de bioinformaticien.ne.s ont travaillé sur le sujet, et de cela beaucoup d'outils avec diverses heuristiques et stratégies ont été dévelloppés, avec des avantages et des inconvénients.  

C'est cette diversité d'outils, et en particulier le travail de Coralie Rohmer sur un pipeline visant à tester et a comparer les outils d'alignement multiple qui a inspiré la question à laquelle mon stage tente de répondre, à savoir est-ce qu'en faisant une synthése des résultats individuels des outils de MSA, sous la forme d'un Méta-Consensus, on obtiens un résultat de meilleure qualité que celui obtenu par n'importe lequel de ces outils....
Dans la suite du rapport, je vais vous exposer le fonctionnement du travail que j'ai réalisé, puis montrer les résultats obtenus.

### Représentation des séquences:

Il existe différentes façon de stocker les séquences, avec plus ou moins d'informations annexes.
La plus simple, et celle que nous utilisons le plus dans notre pipeline est le fasta. Ce format est composé seulement d'une ligne de header commencée par un ">" qui contiens un identifiant pour chaque séquence et peut également contenir des informations telles que le nom de la séquence, le brevet, le numéro d'accession selon l'origine de la séquence. Chaque banque de donnée semble avoir sa propre nomenclature, ce qui rends cette ligne difficilement utilisable pour autre chose que de différencier les séquences. La seconde ligne corresponds à la séquence littérale correspondant au header, utilisant le code IUPAC. C'est un format simple à utiliser et parser et à compresser, ce qui explique son utilisation.
De par la taille que peuvent prendre ce genre de fichiers (plusieurs milliers de bases, avec un nombre de séquences important), de nombreuses séquences sont compressées au format fasta.gz, avec de nombreux outils incluant la décompression des séquences dans leur fonctionnement. 

Le format fasta a été étendu pour contenir plus d'informations, sous le nom de fastq. 
Ce format contiens 4 lignes par séquence:
 - le header, commencé par un "@"
 - la séquence en lettre
 - un "+" pour différencier des headers
 - le valeur de qualité pour chaque base. Ces scores de qualités sont évalués avec la formule:  -10(log[10](p)) avec la probabilité qu'un base soit mauvaise. Ces scores varient largement en fonction de la technologie de séquencage d'origine.
Ce format double la taille nécessaire pour stocker une séquence, ce qui a un coût non négligeable sur l'échelle des bases de données biologiques.

L'autre format utilisé au sein du pipeline est le Sequence Alignment Map (SAM), qui est utilisé pour représenter les alignements de plusieures séquences par rapport à une référence
Ce format est organisé pour chaque séquence en le header de la séquence d'origine en fasta, commencée par un "@" puis d'une ligne représentant l'alignment, composé de 11 items à savoir le nom de la requéte, un flag sur 12 bits détaillant des informations sur la qualité, le succés, l'appairement avec un autre read... pour l'alignement de cette séquence, le nom de la séquence de référence sur laquelle le read est aligné, la position de la première à partir de laquelle la séquence est alignée, un score de qualité du mapping, une chaine CIGAR,  le nom du prochain read aligné, la position à partir de laquelle le prochain read est aligné, puis les chaines du format fastq, c'est a dire la séquence et la qualité de celle-çi. Le format CIGAR consiste en une description des alignements des bases, alternant une lettre et un chiffre. Le code CIGAR est détaillé dans la figure suivante $($$)
Ce format est encore plus lourd que le format fastq, mais porte le plus d'informations. Dans le cadre du pipeline, ces informations sont utilisés pour écarter les reads trop courts, ou qui n'ont pas réussi à s'aligner, et si le travail porte sur une région spécifique, à prendre en compte uniquement les reads suffisament longs pour la région donnée.

##    Déroulé pipeline

L'outil que j'ai développé est un pipeline prenant en entrée des reads au format fasta et produisant en sortie une séquence méta-consensus, elle aussi au format fasta.
Le cheminement des reads jusqu'au résultat est le suivant:
   - Les reads sont d'abords mis sous la forme d'une pile de reads, c'est à dire alignés de façon rapide,
   - Cette pile est ensuite alignée par plusieurs outils de MSA
   - pour chaque MSA, un (ou plusieurs) consensus sont réalisés, produisant une séquence consensus par MSA.
   - ces séquences sont de nouveau empilées, puis à nouveau transformé en "méta-MSA"
   - de cette méta-MSA, on crée le méta-consensus qui est le résultat.

### Création Pile :

Pour la création de la pile, le pipeline implémente deux stratégies, selon si une référence est fournie ou non.

Si une référence est fournie en entrée en plus des reads, nous utilisons l'outil minimap2 pour réaliser un alignement sur la référence donnée.
En cas d'absence de référence, le pipeline extrait le read le plus long du fichier de reads à l'aide d'un script python, puis essaye d'utiliser ce read pour générer un alignement à l'aide de minimap2. Il y a encore du travail de traitement sur ce cas qui doit être réalisé, mais nos test nous permettent d'avoir un alignement relativement proche de celui généré à l'aide d'une référence. Cela viens du fait que minimap2 utilise une comparaison entre read pour améliorer la qualité de l'alignement.

La sortie de minimap2 est au format SAM (Sequence Alignment Map). Ce format relativement complexe contiens une liste de données sur chaque read aligné sur la référence, notamment la position de départ, la position du read suivant, la séquence exacte, la qualité et les différences. 

Les reads qui n'ont pas pu être alignés car trop courts,  ou de trop mauvaise qualité sont écartés dans cette phase, ce qui permet de ne garder "que" des reads exploitables.

Depuis cet alignement au format SAM, le pipeline utilise un script Perl pour extraire sous forme de pile l'ensemble des reads. Ce script permet de découper les reads en régions de longueur variable, ce qui peut avoir un intêret, car la taille des séquence influence fortement la complexité en temps de l'execution des outils de MSA. Il est possible de configurer pour des régions spécifiques, pour toute la séquence découpée en plusieures régions, ou par défaut sur la séquence compléte. 
Qu'il extraie l'intégralité de l'alignement ou uniquement les reads pré-découpés, on obtiens une pile de reads au format fasta avec des positions relative.

Un autre script s'occupe de découper cette pile pour en extraire une certaine profondeur. L'idée est de pouvoir régler les tailles des requêtes spécifiques, toujours afin de pouvoir alléger le coût de l'exécution des outils de MSA. Ce réglage est nécessaire, car comme mis en avant lors de l'explication des MSA, c'est cette profondeur qui correspond à la puissance dans la complexité, et il est donc (très) long d'utiliser un profondeur trop élevée. 

### MSA

De cette pile de reads, le pipeline génére les alignements pour chacun des outils choisis. Notre pipeline implémente à l'heure actuelle 6 outils pour générer les MSA, avec des avantages et des inconvénients. Le choix des outils gardés ou mis de coté est inspiré du travail de Coralie Rohmer, dont le pipeline a prouvé que les outils TCofee et Clustal n'étaient pas adaptés à l'usage sur des longs reads. Les outils supportés par le pipeline sont : Muscle, Mafft, Kalign2, Kalign3, Spoa et Abpoa. 

Le choix de garder ou de supprimer l'utilisation de Muscle de la liste est complexe, car c'est à la fois celui qui donne les meilleurs résultats, et celui qui prends le plus de temps, avec des durées de calcul pouvant monter à plusieurs dizaines d'heures pour des jeux de données modérément grands.

Le choix des outils utilisés est configurable, et l'ajout d'outils est relativement abordable, grace à Snakemake et Conda. En effet, il suffit de générer un environnement Conda (un fichier spécifiant quels outils et ressources doivent être installés pour l'utilisation de l'outil souhaité, au format yaml), de l'inclure dans le dossier workflow env, de créer une régle pour l'outil en suivant le template donné à la fin des régles pour les MSA dans le Snakefile, et de l'ajouter dans le fichier de configuration.

Cette partie est la plus couteuse en temps et en espace de l'ensemble du pipeline, en raison de la complexité de la réalisation de MSA.

### Consensus individuels

Une fois toutes ces MSA réalisées, l'étape suivante consiste à réaliser un consensus pour chacun de ces outils.

La décision qui a été faite pour la notation de la séquence consensus est l'utilisation d'une version étendue du code IUPAC . Le code IUPAC est un format utilisé pour représenter sous forme de lettre les 4 bases, et l'ensemble des "combinaisons" de ces bases pour les cas ou l'on aurait un SNP, et donc deux ou plus bases différentes présentes à l'emplacement donné. Cependant, il a semblé intéressant de pouvoir mettre en avant la présence d'un "gap", car cela peut également constituer un SNP, et donc être partie intégrante d'un variant. Pour ce faire, la décision qui a été prise est d'utiliser lles minuscules pour représenter le choix "gaps"/base. Le code est détaillé dans la figure qui suit. $Code IUPAC étendu $

 Pour réaliser ce consensus, j'ai donc dévelloppé un outil en C++ qui prends une MSA au format fasta en entrée et qui sort une séquence consensus. A l'heure actuelle, deux approches ont été implémentée.

La première est une stratégie naive, qui consiste à compter le nombre de présence de chacune des bases, puis, dans l'ordre décroissant de nombre présence, vérifier si cette valeur dépasse le seuil. Si elle dépasse le seuil, on renvoie la base seule, sinon on stocke la base et son score , on prends la base suivante en ajoutant son score et le total des précédentes, et ainsi de suite jusqu'à ce que le seuil soit atteint, et on renvoie le groupe de bases, transformées ensuite en code IUPAC étendu.

La seconde stratégie reste naive, mais économise sur le nombre d'itération par position. La comparaison entre le seuil et le score le plus haut comme condition de la boucle qui incrémente le score, ce qui fait qui permet d'arrêter le parcours plus tôt si le seuil est atteint par une base avant la fin de la dernière position. Comme le taux d'erreur est inférieur à 10% et que la grande majorité des bases est la même pour une position donnée (chez l'Humain, on trouve un SNP tous les 1000 bases environ), il semble cohérent d'essayer d'arréter le parcours en profondeur plus tôt.

L'outil est composé d'une classe mère MSA et d'une classe fille implémentant une stratégie de consensus. Cela permet de facilement implémenter de nouvelles stratégies pour le consensus, en appliquant le polymorphisme. Il suffit de créer une nouvelle classe étendant la classe MSA avec une méthode consensus pour surcharger la stratégie utilisée. C'est l'organisation la plus légére, ce qui m'a semblé raisonnable pour un outil relativement 'simple'.

Le pipeline génére donc les consensus pour chaque outil de MSA en utilisant mon outil de consensus, produisant des fichiers de consensus intermédiaires. La décision de conserver les consensus individuels séparés permet de faire plus facilement des comparaisons entre ces consensus et la référence, afin de pouvoir étudier l'efficacité réele du pipeline en fin de pipeline. 
Ces fichiers sont ensuite fusionnés en copiant le contenu de chacun des fichiers vers le fichier de sortie, ce qui génére une pile de consensus, toujours au format fasta. 


### Méta-MSA

Création d'une MSA à partir de la pile de consensus

Afin de pouvoir créer la "Méta-MSA", il a fallu choisir un outil pour la générer. Nous sommes conscients que cela rajoute un biais quand à la méthode d'alignement, mais c'est nécessaire pour obtenir un résultat. J'ai pris la décision d'utiliser l'outils Muscle, qui s'est montré particulièrement précis lors de nos expérimentations, et qui avait l'avantage d'être conçu pour l'alignement plus général de protéines, ce qui permet de supporter le code IUPAC fourni en entrée par mon outil de consensus. 
Cette partie pourra donner lieu à des modifications faciles, car le choix de l'outil est paramétrable et fait partie du fichier de configuration de base. 
Cela a été fait des le début, ayant testé également l'outil mafft, mais la sortie de cet outil étant exclusivement en minuscule, cela à pour effet de supprimer la posibilité d'utiliser le code IUPAC étendu, et risquais de mener à l'apparition de faux gaps, et pire, à la disparition des bases pour lesquelles il existait un doute entre plusieures base, car les gaps seraient forcément les plus nombreux dans ce cas.

### Méta-consensus

Avec la méta-MSA fournie, on applique à nouveau l'outil de consensus, avec un seuil différent, afin d'obtenir la séquence finale.

La question du seuil utilisé pour ce Méta-consensus est un facteur important dans la taille de la séquence finale, et dans la correction mutuelle des positions. En effet, comme on a relativement peu de de consensus intermédiaire, la part d'une erreur ou d'un échec d'alignement dans ce consensus se répercute forcément. De plus, les tailles des séquences alignées peuvent varier, selon l'outil utilisé, avec des outils qui favorise l'alignement des bases, ou l'ajout de gaps dans le cas de polyploïdie par exemple. Pour ces raisons, faire varier le seuil utilisé pour le méta-consensus semble permettre de faire un tradeoff entre la taille alignée et la précision finale.


### Plots (optionnel)

Une possibilité offerte par le pipeline est de réaliser des statistiques sur la qualité du méta-consensus, et des consensus intermédiaires. Pour cela, il est cependant nécessaire d'avoir une référence avec laquelle réaliser la comparaisons. L'outil utiliser pour aligner deux à deux la référence et le méta-consensus (ou la référence et chacun des consensus intermédiaires) est exonerate.
Cet outil permet d'obtenir l'alignement complet le plus précis possible, et d'extraire les statistiques de correspondance, similarités, longueurs alignées, nombre de bases correspondante, et non-correspondantes. Cela correspond au données qui ont semblé les plus centrales. Un point qui est envisagé pour la suite est de réussir à extraire les régions autour des mismatches pour pouvoir étudier plus précisément quelles sont les raisons derriére ces mismatches, en extrayant les quelques bases et la position, et en les comparant avec les positions équivalentes sur les consensus intermédiaires. L'avantage d'exonerate est qu'il "comprends" le code IUPAC standard, et qu'il arrive a reconnaitre un match "partiel" si pour une position donnée il y a une base et le code IUPAC correspondant à cette base et une (ou 2 autres). Cependant, cela ne prends malheusement pas en compte l'idée du code IUPAC étendu, considérant comme identique une lettre minuscule ou majuscule, ce qui reste néanmoins mieux qu'un mismatch total.

## Choix techniques

Le travail que j'ai mené est donc l'écriture d'un code permettant de suivre ces 5 grandes parties en générant les fichiers au fur à mesure, en traitant potentiellement les fichier à l'aide de scripts intermédiaires quand c'était nécessaire. Le gros du pipeline a été réalisé en Snakemake, avec un outil en C++ et des scripts en shell et Python. 

Snakemake est un outil conçu pour écrire des workflow, c'est-à-dire concevoir un chaînage de règles permettant de générer un ou plusieurs fichier, a la manière d'un Makefile. 
Le fonctionnement est simple, le snakefile va rechercher l'ensemble des fichiers demandés a la sortie de la règle finale, et remonter de règles en règles pour générer les ressources manquantes. L'un des avantages majeurs que propose Snakemake est l'inclusion d'un système de "wildcards". Ce système permet de modulariser les emplacements, noms, etc des fichier, des valeurs de paramètres pour les commandes, bref soit de les déduire, soit de les tirer du fichier de configuration. Ainsi, en changeant simplement le(s) fichier(s) d'entrée, et/ou des paramètres généraux, on peut utiliser le même Snakefile pour générer un ensemble de fichiers différents. Dans notre cas, l'intérêt de cette fonctionnalité est qu'en modifiant un fichier de configuration simple, on peut appliquer le comportement a plusieurs jeux de données, et ce en même temps.
L'autre avantage majeur qui s'applique dans le cadre de ce projet est la facilité que Snakemake permet pour la parrallelisation, l'ordonancement et la clusterisation de tâches, ce qui est important en bioinformatique, compte tenu de la durée que peuvent prendre certaines tâches, et de la quantité de données qu'il faut gérer, les outils doivent pouvoir être lancés de manière indépendante, et poursuivre leur cheminement.

Pour la création de l'outil de consensus, j'ai décidé d'utiliser le C++ pour plusieures raisons . Tout d'abord, une base de code sur la gestion de MSA, réalisée par Coralie, m'avait été fournie dès le début, ce qui m'a permis d'avoir une idée du cheminement et d'une façon de réaliser la tâche. De plus, le C++ est un langage bien plus efficace que le Python sur la gestion de la mémoire et la rapidité d'exécution, ce qui m'a d'autant plus motivé à le dévellopper en C++. L'approche objet permet, via le polymorphisme, de pouvoir facilement créer des classes filles qui implémentent d'autres stratégies de consensus, ce qui est un atout pour l'évolution possible de l'outil. Les MSA sont stockés sous la forme d'un vecteur de chaine de caractères, ce qui permet de naviguer avec de coordonées de la chaine de caractère pour la profondeur et du caractère pour la position. La représentation de l'alphabet IUPAC étendu, il est stocké sous la forme d'une chaine de caractère, en organisant les lettres de façon à ce que chaque bit de l'index corresponde à la présence d'une base (ou d'un gap). Le choix réalisé est , par poids du bit croissant, A = 1, C = 2, G = 4, T = 8, et le gap = 16. Cela permet la conversion vers et depuis le code IUPAC étendu en utilisant un masque adapté.

Globalement, la grande majorité des scripts utilisés pour modifier les sorties sont réalisés soit en Python si le scripts demandait une itération précise, de la gestion ou de la modification de variable, soit en bash si le scripts utilisait des fonctions de bases du noyau Unix et des redirections simple, afin d'éviter d'ajouter une surcouche avec le Python si celle-çi n'était pas nécessaire. 
Pour l'édition de plots, j'ai utilisé la librairie Matplotlib, une librairie Python dédiée. Le script doit cependant être mis à jour et amélioré, car il s'agit pour l'instant d'une version de débug, réalisée rapidement afin d'avoir un retour visuel plus direct sur les résultats d'expérimentations, mais qui ne sont difficilement exploitables en l'état.

## Difficultés rencontrées.

Comme la taille des reads d'ADN est de l'ordre de plusieurs milliers de bases, et que l'on essaye de conserver un nombre de read élevé, le coût en espace et/ou en temps des outils utilisés pour les outils de MSA augmente drastiquement, au delà des capacités de la plupart des postes de travail. C'est notamment le cas de Abpoa et Spoa, qui échouent dès l'allocation des ressources necessaires sur mon poste de travail notamment pour un échelle de plus de 40000 bases et une profondeur de 30. 
Muscle quand a lui a des durées d'exécutions dépassant les 8 heures pour ce cas ce qui m'a pas laissé l'occasion de le lancer au bureau et j'ai du coup pas fini la run. 
Cependant, Muscle ayant des résultats visiblement supérieurs aux autres outils, il semblait intéressant d'essayer de le conserver. 
De plus, après avoir écarté Spoa, Abpoa et Muscle, cela nous laisse un meta-consensus sur 3 outils, ce qui réduit encore plus la qualité générale du résultat.
Pour apboa et spoa, on peut imaginer que lancer le pipeline avec plus de mémoire vive disponible pourrait résoudre le probléme, mais la question, de la consommation en temps de Muscle, qui domine de loin le temps d'exécution du pipeline est plus complexe.

## Test effectués

Des runs du pipeline ont été réalisés au fur et a mesure, d'abords avec des données courtes pré-découpées et déja organisées en MSA, créées en fusionnant les reads pour deux jeux de séquences de levures, et leurs références, afin de créer un "faux-diploïde". Ce jeu de données a permis de faire un travail en partant de la fin, c'est a dire la génération de consensus, l'alignement de ceux-çi et la création du méta-consensus, et d'ensuite remonter jusqu'au traitement des reads. Une fois le pipeline complété pour le fonctionnement avec une référence, on m'a fourni les reads et la référence d'origine. J'ai donc pu confirmer le bon fonctionnement du début de pipeline en comparant les MSA réalisée par mon pipeline à celles qui m'avaient été fournies initialement, qui ont donné le même résultat. Puis sont venues les expérimentations sur la partie "alignment sur le read le plus long". Sur ce point, j'ai rencontré des résultats étranges sur l'alignement, j'ai donc travaillé sur un jeu de donné haploïde, plus 'simple', et je n'ai pas pris le temps de revenir sur ce jeu de données.  Après deux runs de pipeline sur la même région, l'un avec la référence, l'autre sans, j'ai pu comparer les résultats obtenus par l'assemblage. Malgrè une taille alignée inférieure, on obtiens une similarité proche de 100%

\pagebreak

# Conclusion (1p)

$ Je sais pas quoi dire là, j'ai l'impression de déja beaucoup avoir recyclé les mêmes concepts $ 
Le pipeline fonctionne, est simple d'utilisation et facilement paramétrable et modifiable. Ce rapport arrive avant que le travail ait pu être entièrement terminé, il reste de nombreuses possibilités de modification et d'amélioration, sur lesquels j'aurai la chance de pouvoir continuer de travailler sur le mois qu'il me reste en stage. $

L'objectif in fine du travail réalisé sur ce pipeline est d'avoir un outil utilisable $

# Bilan (0.5p)

Je suis très satisfait de cette expérience, tant sur un plan professionel que personnel. J'ai eu l'occasion d'avoir une expérience concrete du monde de la recherche public, avec notament l'occasion d'assister à des conférences et présentations de travaux de recherche, ce qui est une chance à mes yeux, et n'a fait que renforcer mon intéret et mes aspirations à faire de la recherche mon projet professionel. J'ai également eu la chance de découvrir Snakemake, et étudié de nombreux concepts d'algorithmie des séquences, qui me semble être un outil qui permettent de générer efficacement des données spécifiques, ce qui pourra être utile dans le cadre d'expériences, notament dans le cadre de mon Master Machine Learning, pour des données d'entrée par exemple. J'ai également eu l'occasion de commencer la pratique du C++, qui me semble être un langage qu'il pourrait m'intéresser de creuser plus à l'avenir. 
Sur le plan personnel, j'ai eu la chance de travailler au sein d'une équipe accueillante, tout en ayant une autonomie élevée tout le long de mon stage. L'autonomie a impliqué un certain nombre de prise de décision, et une gestion des responsabilités, qui m'a aidé à dévellopper ma capacité à travailler et à expérimenter seul, sans pour autant être isolé, avec la possibilité de discuter de sujets intéressants avec des spécialistes de leur domaine. J'ai également appris à faire attention à anticiper la taille que peut prendre le travail sur des données biologique, grâce à une tentative de comparaison entre un groupe d'une dizaine de milliers de reads et lui-même (environ 5 Gb de données) qui a remplis en l'espace d'une minute l'intégralité de la mémoire disque disponible sur mon poste de travail (120 Gb à ce moment la). 

# Hors-sujet : DPP, Fresque du climat (1p - 5p)




# Biblio

## Outils

Conda : Anaconda Software Distribution. Conda. Version 2-2.4.0, Anaconda, Nov. 2016. Computer Software. Anaconda, www.anaconda.com. 
Snakemake : https://doi.org/10.12688/f1000research.29032.2
Mafft : https://doi.org/10.1093/molbev/mst010 (v7 ?)
Kalign2 :  10.1093/nar/gkn1006 (22 dec 2008)
Kalign3 : https://doi.org/10.1093/bioinformatics/btz795
Spoa : 10.1101/gr.214270.116
Abpoa :  10.1093/bioinformatics/btaa963 
Exonerate : doi: 10.1186/1471-2105-6-31
Minimap2 : https://doi.org/10.1093/bioinformatics/bty191
samtools : DOI: 10.1093/gigascience/giab008
Coralie Rohmer Thèse ?

## Mentions

Génome Humain complet:  DOI: 10.1126/science.abj6987


# Annexes: détail.

Lien github: *insert*

